terraform {
  required_version = ">= 0.8"
}

provider "aws" {
  region = "us-east-1"
}

resource "aws_db_instance" "example" {
  engine            = "mysql"
  allocated_storage = 10

  instance_class      = "${var.db_instance_class}"
  name                = "${var.db_name}"
  username            = "admin"
  password            = "${var.db_password}"
  skip_final_snapshot = true
}
