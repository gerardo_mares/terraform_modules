output "address" {
  value = "${aws_db_instance.example.address}"
}

output "db_name" {
  value = "${aws_db_instance.example.db_name}"
}

output "port" {
  value = "${aws_db_instance.example.port}"
}
