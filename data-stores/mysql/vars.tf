variable "db_password" {
  description = "The password for the database"
}

variable "db_name" {
  description = "The name to use for the database"
}

variable "db_instance_class" {
  description = "The instanc type to use for the database"
}
